@extends('layouts.admin')

@section('admincontent')
    
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="flex-wrap">
                    <div class="card">
                        {{-- <div class="card-header">{{ __('Register') }}</div> --}}
                        <div class="card-header">
                            <a href="/">
                                <img src="/images/icon/logo-mini.png" alt="company name">
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="card-title">
                                <h3 class="text-center title-2">User group 1</h3>
                            </div>
                            <form method="POST" action="{{ route('usergroups.update', 1) }}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                                @include('includes.forminputfields',['fieldname' => 'name', 'fieldtype' => 'text', 'attributes' => 'required autofocus'])

                                @include('includes.forminputfields',['fieldtype' => 'submit', 'fieldlabel' => 'Edit user group'])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>

@endsection
