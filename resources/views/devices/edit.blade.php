@extends('layouts.admin')

@section('admincontent')
    
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="flex-wrap">
                    <div class="card">
                        {{-- <div class="card-header">{{ __('Register') }}</div> --}}
                        <div class="card-header">
                            <a href="/">
                                <img src="/images/icon/logo-mini.png" alt="company name">
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="card-title">
                                <h3 class="text-center title-2">Device 1</h3>
                            </div>
                            <form method="POST" action="{{ route('devices.update', $device) }}">
                                @csrf
                                @method('PUT')

                                @include('includes.forminputfields',['fieldname' => 'name', 'fieldtype' => 'text', 'fielddefault' => $device->name, 'attributes' => 'required autofocus'])

                                @include('includes.forminputfields',[
                                    'fieldname' => 'is_active',
                                    'fieldtype' => 'select', 
                                    'fieldlabel' => 'Status',
                                    'fielddefault' => $device->is_active,
                                    'fieldoptions' => [
                                        '0' => 'Disabled',
                                        '1' => 'Active'
                                    ],
                                    'attributes' => 'required'
                                ])

                                <div class="form-group row">
                                    <p class="text-center">
                                        @if ($errors->has('device_group_ids.*'))
                                            <span class="text-danger error">
                                                <strong>{{ $errors->first('device_group_ids.*') }}</strong>
                                            </span>
                                        @endif
                                    </p>
                                    <div class="table-responsive m-b-40">
                                        <table class="table table-borderless table-data2 datatable">
                                            <thead>
                                                <tr>
                                                    <th>Assign to Device Groups (Optional)</th>
                                                    <th>Device count</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($devicegroups as $devicegroup)
                                                    <tr>
                                                        <td>
                                                            <div class="form-group row">
                                                                <div class="col-md-1">
                                                                    <input id="group{{$devicegroup->id}}" type="checkbox" class="form-check-input" {{($devicegroup->devices->contains($device)) ? 'checked' : ''}} name="device_group_ids[]" value="{{$devicegroup->id}}">
                                                                </div>
                                                                <label for="group{{$devicegroup->id}}" class="form-check-label col-md-4">{{$devicegroup->name}}</label>
                                                            </div>
                                                        </td>
                                                        <td>{{$devicegroup->devices_count}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                @include('includes.forminputfields',['fieldtype' => 'submit', 'fieldlabel' => 'Edit device'])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>

@endsection
