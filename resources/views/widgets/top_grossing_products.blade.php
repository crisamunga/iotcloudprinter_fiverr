<div class="table-responsive">
    <table class="table table-top-campaign">
        <tbody>
            @foreach ($top_products as $product)
                <tr>
                    <td>{{$product->name}}</td>
                    <td>{{$product->gross}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>