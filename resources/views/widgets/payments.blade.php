<div class="col-lg-6">
    <div class="au-card chart-percent-card">
        <div class="au-card-inner">
            <h3 class="title-2 tm-b-5">Payments</h3>
            <div class="row no-gutters">
                <div class="col-xl-6">
                    <div class="chart-note-wrap">
                        <div class="chart-note mr-0 d-block">
                            <span class="dot dot--blue"></span>
                            <span>credit card</span>
                        </div>
                        <div class="chart-note mr-0 d-block">
                            <span class="dot dot--red"></span>
                            <span>cash</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="percent-chart">
                        <canvas id="percent-chart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>