@component('components.statisticcard')
    @slot('icon')
        <i class="zmdi zmdi-shopping-cart"></i>
    @endslot
    <h2>{{$total_sales}}</h2>
    <span>Total lifetime sales</span>
@endcomponent