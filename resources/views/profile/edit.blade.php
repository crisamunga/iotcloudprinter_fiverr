@extends('layouts.admin')

@section('admincontent')
    
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="flex-wrap">
                    <div class="card">
                        {{-- <div class="card-header">{{ __('Register') }}</div> --}}
                        <div class="card-header">
                            <a href="/">
                                <img src="/images/icon/logo-mini.png" alt="company name">
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="card-title">
                                <h3 class="text-center title-2">{{AUth::user()->name}}</h3>
                            </div>
                            <form method="POST" action="{{ route('profile.update') }}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                                @include('includes.forminputfields',['fieldname' => 'name', 'fielddefault' => Auth::user()->name, 'fieldtype' => 'text', 'attributes' => 'required autofocus'])

                                @include('includes.forminputfields',['fieldname' => 'email', 'fielddefault' => Auth::user()->email, 'fieldtype' => 'email', 'attributes' => 'required'])

                                @include('includes.forminputfields',['fieldname' => 'picture', 'fieldlabel' => 'Picture (max 5mb)', 'fieldtype' => 'image'])

                                @include('includes.forminputfields',['fieldtype' => 'submit', 'fieldlabel' => 'Update profile'])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>

@endsection
