@extends('layouts.admin')

@section('admincontent')
<h3 class="title-5">{{$user->name}}</h3>
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-user"></i>
                            <strong class="card-title pl-2">Profile</strong>
                        </div>
                        <div class="card-body">
                            <div class="mx-auto d-block">
                                <img class="rounded-circle mx-auto d-block" src="{{$user->imageUrl('picture')}}" alt="Card image cap">
                                <h3 class="text-sm-center mt-2 mb-1 title-1">{{$user->name}}</h3>
                                <div class="location text-sm-center"><i class="fa fa-envelope"></i> {{$user->email}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <!-- TOP CAMPAIGN-->
                    <div class="top-campaign">
                        <h3 class="title-3 m-b-30">{{$user->name}}</h3>
                        <div class="table-responsive">
                            <table class="table table-top-campaign">
                                <tbody>
                                    <tr>
                                        <td>Name</td>
                                        <td>{{$user->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>
                                            <span class="status--{{$user->is_active ? 'process' : 'denied'}}">{{$user->is_active ? 'Active' : 'Disabled'}}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Device limit</td>
                                        <td>{{$user->device_limit > 0 ? $user->device_limit : 'Not set'}}</td>
                                    </tr>
                                    <tr>
                                        <td>Devices</td>
                                        <td>{{$user->devices->count()}}</td>
                                    </tr>
                                    <tr>
                                        <td>Date added</td>
                                        <td>{{$user->created_at}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center">
                            <a class="au-btn au-btn-icon au-btn--blue2 au-btn--small" href="{{route('users.edit', $user)}}"><i class="zmdi zmdi-edit"></i>edit user</a>
                        </div>
                    </div>
                    <!-- END TOP CAMPAIGN-->
                </div>
            </div>
        </div>
    </div>
</div>

@include('includes.devices', ['devices' => $user->devices, 'hide_add_device_link' => true])
@endsection
