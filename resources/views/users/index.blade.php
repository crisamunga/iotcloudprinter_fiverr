@extends('layouts.admin')

@section('admincontent')

<div class="row">
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">Users</h3>
        
        <div class="table-responsive table-responsive-data2">
            <table class="table table-data2 datatable">
                <thead>
                    <tr>
                        <th>name</th>
                        <th>email</th>
                        <th>groups</th>
                        <th>Status</th>
                        <th>devices</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr class="tr-shadow">
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                <ul>
                                    <li><span class="block-email">Super Admin</span></li>
                                    <li><span class="block-email">Group admin</span></li>
                                    <li><span class="block-email">Device admin</span></li>
                                </ul>
                            </td>
                            <td>
                                <span class="status--{{$user->is_active ? 'process' : 'denied'}}">{{$user->is_active ? 'Active' : 'Disabled'}}</span>
                            </td>
                            <td>{{$user->devices_count}}</td>
                            <td>
                                <div class="table-data-feature">
                                    <a href="{{route('users.edit', $user)}}" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>
                                    <form action="{{route('users.destroy',$user)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </button>
                                    </form>
                                    <a href="{{route('users.show', $user)}}" class="item" data-toggle="tooltip" data-placement="top" title="More">
                                        <i class="zmdi zmdi-more"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
</div>

@endsection
