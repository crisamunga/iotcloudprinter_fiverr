<a href="{{ route('logout') }}"
    onclick="event.preventDefault();
                    document.getElementById('__user-logout-form').submit();">
    <i class="zmdi zmdi-power-off"></i>
    {{ __('Logout') }}
</a>

<form id="__user-logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>