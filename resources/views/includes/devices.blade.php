<!-- DATA TABLE -->
<h3 class="title-5 m-b-35">Devices</h3>
<div class="table-data__tool">
    <div class="table-data__tool-left">
        <a class="au-btn au-btn-icon au-btn--blue2 au-btn--small" href="{{route('devices.export')}}" target="_blank">
            <i class="fa fa-file-excel"></i> Export CSV
        </a>
    </div>
    <div class="table-data__tool-right">
        @if (!isset($hide_add_device_link) || !$hide_add_device_link)
            <a class="au-btn au-btn-icon au-btn--green au-btn--small" href="{{route('devices.create')}}">
                <i class="zmdi zmdi-plus"></i>add device
            </a>
        @endif
    </div>
</div>
<div class="table-responsive table-responsive-data2">
    <table class="table table-data2 datatable">
        <thead>
            <tr>
                <th>name</th>
                <th>date added</th>
                <th>Status</th>
                <th>Sales</th>
                <th>groups</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($devices as $device)
                <tr class="tr-shadow">
                    <td>{{$device->name}}</td>
                    <td>{{$device->created_at}}</td>
                    <td>
                        <span class="status--{{$device->is_active ? 'process' : 'denied'}}">{{$device->is_active ? 'Active' : 'Disabled'}}</span>
                    </td>
                    <td>{{$device->sales->sum('total')}}</td>
                    <td>
                        <ul class="list-unstyled">
                            @foreach ($device->device_groups as $device_group)
                                <li class="m-b-20">
                                    <a href="{{route('devicegroups.show', $device_group)}}">
                                        <span class="block-email">{{$device_group->name}}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </td>
                    <td>
                        <div class="table-data-feature">
                            <a href="{{route('devices.edit', $device)}}" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                            <form action="{{route('devices.destroy', $device)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="item" type='submit' data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                            </form>
                            <a href="{{route('devices.show', $device)}}" class="item" data-toggle="tooltip" data-placement="top" title="More">
                                <i class="zmdi zmdi-more"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                <div class="spacer"></div>
            @endforeach
        </tbody>
    </table>
</div>
<!-- END DATA TABLE -->