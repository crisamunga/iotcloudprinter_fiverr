@extends('layouts.admin')

@section('admincontent')

<h3>{{$devicegroup->name}}</h3>
@include('includes.devices', ['devices' => $devicegroup->devices, 'hide_add_device_link' => true])

@endsection
