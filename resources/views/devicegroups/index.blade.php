@extends('layouts.admin')

@section('admincontent')

<div class="row">
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">Device groups</h3>
        <div class="table-data__tool">
            <div class="table-data__tool-left">
                <a class="au-btn au-btn-icon au-btn--blue2 au-btn--small" href="{{route('devicegroups.export')}}" target="_blank">
                    <i class="fa fa-file-excel"></i> Export CSV
                </a>
            </div>
            <div class="table-data__tool-right"></div>
        </div>
        <div class="table-responsive table-responsive-data">
            <table class="table table-data2 datatable">
                <thead>
                    <tr>
                        <th>name</th>
                        <th>devices</th>
                        <th>Sales</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($devicegroups as $devicegroup)
                        <tr class="tr-shadow">
                            <td>{{$devicegroup->name}}</td>
                            <td>
                                <span class="status--{{$devicegroup->devices_count > 0 ? 'process' : 'denied'}}">{{$devicegroup->devices_count}}</span>
                            </td>
                            <td>{{$devicegroup->total_sales}}</td>
                            <td>
                                <div class="table-data-feature">
                                    <a href="{{route('devicegroups.edit', $devicegroup)}}" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>
                                    <form action="{{route('devicegroups.destroy', $devicegroup)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </button>
                                    </form>
                                    <a href="{{route('devicegroups.show', $devicegroup)}}" class="item" data-toggle="tooltip" data-placement="top" title="More">
                                        <i class="zmdi zmdi-more"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>    
                        <div class="spacer"></div>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
</div>

@endsection
