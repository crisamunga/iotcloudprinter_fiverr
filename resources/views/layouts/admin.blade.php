@extends('layouts.app')

@section('content')
    <!-- HEADER DESKTOP-->
    <header class="header-desktop4">
        <div class="container">
            <div class="header4-wrap">
                <div class="header__logo">
                    <a href="#">
                        <img src="/images/icon/logo-blue.png" alt="CoolAdmin" />
                    </a>
                </div>
                <div class="header__tool">
                    @include('includes.headertool')
                </div>
            </div>
        </div>
    </header>
    <!-- END HEADER DESKTOP -->

    <!-- WELCOME-->
    <section class="welcome2 p-t-40 p-b-55">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @include('includes.breadcrumbs')
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="welcome2-inner m-t-60">
                        <div class="welcome2-greeting">
                            <h1 class="title-6">Hi <span>{{ Auth::user()->name }}</span>, Welcome back</h1>
                            <p>Last logged in {{Auth::user()->last_login}}</p>
                        </div>
                        <form class="form-header form-header2" action="" method="post">
                            {{-- TODO: Add site-wide search functionality --}}
                            {{-- <input class="au-input au-input--w435" type="text" name="search" placeholder="Search for datas &amp; reports...">
                            <button class="au-btn--submit" type="submit">
                                <i class="zmdi zmdi-search"></i>
                            </button> --}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END WELCOME-->

    <!-- PAGE CONTENT-->
    <div class="page-container3 p-b-100 p-t-100">
        @if ($errors->any())
            <section class="alert-wrap p-t-10 p-b-20">
                <div class="container">
                    <!-- ALERT-->
                    @component('components.notification', ['class' => 'danger'])
                        There was an error in your input
                    @endcomponent
                    <!-- END ALERT-->
                </div>
            </section>
        @endif
        @if (session('flash_message'))
            <section class="alert-wrap p-t-10 p-b-20">
                <div class="container">
                    <!-- ALERT-->
                    @component('components.notification', ['class' => session('flash_message_type')])
                        {{session('flash_message')}}
                    @endcomponent
                    <!-- END ALERT-->
                </div>
            </section>
        @endif
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 no-print">
                        <!-- MENU SIDEBAR-->
                        <aside class="menu-sidebar3 js-spe-sidebar">
                            <nav class="navbar-sidebar2 navbar-sidebar3">
                                @include('includes.adminsidebar')
                            </nav>
                        </aside>
                        <!-- END MENU SIDEBAR-->
                    </div>
                    <div class="col-xl-9 print-container">
                        <!-- PAGE CONTENT-->
                        <div class="page-content">
                            @yield('admincontent')
                        </div>
                        <!-- END PAGE CONTENT-->
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    <script>
        $(function(){
            var pathname = window.location.origin+"/"+window.location.pathname.split('/')[1];
            $('a[href="'+ pathname +'"]').parent().parent().parent().addClass('active');

            $('.datatable').each((index, elem) => {
                $(elem).DataTable();
            });
            
            $('.ajaxform').each((index, elem) => {
                $(elem).ajaxform();
            })
        });
    </script>
    @yield('adminscripts')
@endsection