<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');
Route::post('/dashboard/top-grossing-products', 'DashboardController@top_grossing_products')->name('dashboard.top_grossing_products');
Route::post('/dashboard/top-selling-products', 'DashboardController@top_selling_products')->name('dashboard.top_selling_products');
Route::post('/dashboard/sales-gross-chart', 'DashboardController@sales_gross_chart')->name('dashboard.sales_gross_chart');
Route::post('/dashboard/device-sales', 'DashboardController@device_sales')->name('dashboard.device_sales');
Route::get('/dashboard/export-devices-csv', 'DashboardController@export_devices_csv')->name('dashboard.export_devices_csv');

Route::get('/profile', 'ProfileController@index')->name('profile.index');
Route::get('/profile/edit', 'ProfileController@edit')->name('profile.edit');
Route::put('/profile/update', 'ProfileController@update')->name('profile.update');

Route::get('/devices/export', 'DeviceController@export')->name('devices.export');
Route::resource('devices', 'DeviceController');

Route::resource('users', 'UserController', ['except' => ['create', 'store']]);

Route::get('/devicegroups/export', 'DeviceGroupController@export')->name('devicegroups.export');
Route::resource('devicegroups', 'DeviceGroupController');


Route::resource('usergroups', 'UserGroupController');

Route::get('/sales/{sale}/download', 'SaleController@download')->name('sales.download');
Route::get('/sales/export', 'SaleController@export')->name('sales.export');
Route::resource('sales', 'SaleController', ['only' => ['index', 'show']]);

Route::get('/products/export', 'ProductController@export')->name('products.export');
Route::resource('products', 'ProductController', ['only' => ['index', 'show']]);