<?php

namespace App\Http\Controllers;

use App\DeviceGroup;
use Illuminate\Http\Request;
use App\Http\Requests\CreateOrUpdateDeviceGroupRequest;
use Response;

class DeviceGroupController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $devicegroups = DeviceGroup::with('devices.sales')->withCOunt('devices')->get();
        return view('devicegroups.index', compact('devicegroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('devicegroups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOrUpdateDeviceGroupRequest $request)
    {
        $input = $request->validated();
        $devicegroup = DeviceGroup::create($input);
        return redirect()->route('devicegroups.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(DeviceGroup $devicegroup)
    {
        $devicegroup->load('devices');
        return view('devicegroups.show', compact('devicegroup'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(DeviceGroup $devicegroup)
    {
        return view('devicegroups.edit', compact('devicegroup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateOrUpdateDeviceGroupRequest $request,DeviceGroup $devicegroup)
    {
        $input = $request->validated();
        $devicegroup->fill($input)->save();
        return redirect()->route('devicegroups.show', $devicegroup);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeviceGroup $devicegroup)
    {
        $devicegroup->delete();
        return redirect()->route('devicegroups.index');
    }

    public function export()
    {
        $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=devicegroups.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        $devicegroups = DeviceGroup::with('devices')->get();

        $data = $devicegroups->map(function ($devicegroup)
        {
            return [
                $devicegroup->name, 
                $devicegroup->devices->count(), 
                $devicegroup->total_sales,
                $devicegroup->gross_sales,
            ];
        })->toArray();

        array_unshift($data, ['Device Group', 'Device count', 'Sales', 'Gross']);

        $callback = function() use ($data) 
        {
            $FH = fopen('php://output', 'w');
            foreach ($data as $row) { 
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return Response::stream($callback, 200, $headers);
    }
}
