<?php

namespace App\Http\Controllers;

use App\Sale;
use Illuminate\Http\Request;
use Response;

class SaleController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Sale::with('device')->withCount('sale_products')->get();
        return view('sales.index', compact("sales"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show(Sale $sale)
    {
        $sale->load(['sale_products','sale_products.product']);
        return view('sales.show', compact("sale"));
    }

    /**
     * Download the receipt of the specified sale.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function download(Sale $sale)
    {
        if (!isset($sale->receipt)) {
            abort(404);
        }
        return response()->download(storage_path('app/'.$sale->receipt), 'Receipt for sale '.$sale->reference_number);
    }

    public function export()
    {
        $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=sales.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        $sales = Sale::with('device')->withCount('products')->get();

        $data = $sales->map(function ($sale)
        {
            return [
                $sale->sale_date, 
                $sale->reference_number, 
                $sale->device->name,
                $sale->products_count,
                $sale->total,
            ];
        })->toArray();

        array_unshift($data, ['Date', 'Reference number', 'Device', 'Products', 'Total']);

        $callback = function() use ($data) 
        {
            $FH = fopen('php://output', 'w');
            foreach ($data as $row) { 
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return Response::stream($callback, 200, $headers);
    }
}
