<?php

namespace App\Http\Controllers;

use App\Sale;
use App\Device;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Widget;
use Response;

class DashboardController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $devices = Device::all();
        $sales = Sale::all();

        $total_devices = $devices->count();
        $total_sales = $sales->count();

        return view('dashboard.index', [
            'total_devices' => $total_devices,
            'total_sales' => $total_sales,
        ]);
    }

    public function top_selling_products(Request $request)
    {
        $span = $request['span'];

        return Widget::topSellingProducts(['span' => $span]);
    }

    public function top_grossing_products(Request $request)
    {
        $span = $request['span'];

        return Widget::topGrossingProducts(['span' => $span]);
    }

    public function sales_gross_chart(Request $request)
    {
        $span = $request['span'];
        $summary = $request['summary'];

        return Widget::salesGrossChart(['span' => $span, 'summary' => $summary]);
    }

    public function device_sales(Request $request)
    {
        return Widget::deviceSales();
    }

    public function export_devices_csv()
    {
        $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=device_sales.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        $devices = Device::with('sales')->get();

        $data = $devices->map(function ($device)
        {
            return [$device->name, $device->sales->count(), $device->sales->sum('amount')];
        })->toArray();

        array_unshift($data, ['Device', 'Sales', 'Gross']);

        $callback = function() use ($data) 
        {
            $FH = fopen('php://output', 'w');
            foreach ($data as $row) { 
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return Response::stream($callback, 200, $headers);
    }
}
