<?php

namespace App;

use App\Device;
use App\Product;
use Carbon\Carbon;
use App\SalePayment;
use App\SaleProduct;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasUuid;

    public $incrementing = false;
    
    protected $casts = [
        'additional_details' => 'array',
    ];

    protected $fillable = ['total', 'method', 'reference_number', 'receipt', 'additional_details', 'sale_date', 'device_id'];

    public function device()
    {
        return $this->belongsTo(Device::class, 'device_id', 'id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'sale_products', 'sale_id', 'product_id');
    }

    public function sale_products()
    {
        return $this->hasMany(SaleProduct::class, 'sale_id', 'id');
    }

    public function sale_payments()
    {
        return $this->hasMany(SalePayment::class, 'sale_id', 'id');
    }

    public function scopeToday($query)
    {
        return $query->whereDate('sale_date', '=', Carbon::today()->toDateString());
    }

    public function scopeThisWeek($query)
    {
        return $query->whereBetween('sale_date', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
    }

    public function scopeThisMonth($query)
    {
        return $query->whereMonth('sale_date', '=', Carbon::today()->month);
    }

    public function scopeThisYear($query)
    {
        return $query->whereYear('sale_date', '=', Carbon::today()->year);
    }

    public function scopeDaily($query)
    {
        return $query->groupBy(function($sale) {
            return Carbon::parse($sale->sale_date)->format('Y-m-d');
        });
    }
}