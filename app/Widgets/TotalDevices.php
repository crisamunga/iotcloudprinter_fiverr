<?php

namespace App\Widgets;

use App\Device;
use Arrilot\Widgets\AbstractWidget;

class TotalDevices extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $device_count = Device::count();

        return view('widgets.total_devices', [
            'config' => $this->config,
            'device_count' => $device_count,
        ]);
    }
}
