<?php

namespace App\Widgets;

use App\Sale;
use Arrilot\Widgets\AbstractWidget;

class TotalSales extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $total_sales = Sale::count();

        return view('widgets.total_sales', [
            'config' => $this->config,
            'total_sales' => $total_sales,
        ]);
    }
}
