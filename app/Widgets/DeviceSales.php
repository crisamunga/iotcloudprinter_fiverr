<?php

namespace App\Widgets;

use App\Device;
use Arrilot\Widgets\AbstractWidget;

class DeviceSales extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $devices = Device::with('sales')->get();

        return view('widgets.device_sales', [
            'config' => $this->config,
            'devices' => $devices,
        ]);
    }
}
