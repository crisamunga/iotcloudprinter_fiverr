<?php

namespace App\Widgets;

use App\Sale;
use Arrilot\Widgets\AbstractWidget;

class TotalEarnings extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $total_earnings = Sale::sum('total');

        return view('widgets.total_earnings', [
            'config' => $this->config,
            'total_earnings' => $total_earnings,
        ]);
    }
}
