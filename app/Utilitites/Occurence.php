<?php

namespace App\Utilitites;
use Carbon\Carbon;
use Spatie\PdfToText\Pdf;


class Occurence
{
    /**
     * Returns the string representation of occurence based on supplied times e.g. Never, Once, Twice, 10 Times
     * 
     * @param int $times
     */
    public static function get($times)
    {
        if ($times == 0) {
            return "never";
        }
        if ($times == 1) {
            return "once";
        }
        if ($times == 2) {
            return "twice";
        }
        if ($times == 3) {
            return "thrice";
        }
        return $times." times";
    }
}